var MysqlController = {};
var Mysql = require("../model/mysql");
var Mysql_Hour = require("../model/mysql_hour");

var conn = require('./../config/mysqldb')
let arydata = []
let count
let datasql
MysqlController.find = function (res) {
    conn.connect(function (err) {
        if (err) console.log(err);
        else{console.log("connect mysql")}

        var id = 1;
        let y = 1
        let i = 0
        try {



            conn.query('SELECT   G1_PVVoltage,G1_PVCurrent,G1_PVPower_kW,Wind_Speed_ms,Wind_Direction,G1_ACToday_notHold,DatetimeSMU1 FROM (SELECT G1_PVVoltage,G1_PVCurrent,G1_PVPower_kW,Wind_Speed_ms,Wind_Direction,G1_ACToday_notHold,DatetimeSMU1 FROM main_table ORDER BY DatetimeSMU1 DESC LIMIT 5000)sub ORDER BY DatetimeSMU1 ASC', function (err, rows) {
                if (err) {
                    console.log(err);
                }
                for (id in rows) {


                     // console.log(rows)
                        var dateformat = require('dateformat')
                     
                        var mysql = new Mysql({

                            currents: rows[id].G1_PVCurrent,
                            voltage: rows[id].G1_PVVoltage,
                            power: rows[id].G1_PVPower_kW,
                            energy: rows[id].G1_ACToday_notHold,
                            wind_speed: rows[id].Wind_Speed_ms,
                            wind_direction: rows[id].Wind_Direction,
                            timestamp:dateformat(rows[id].DatetimeSMU1.toString(),  "isoDateTime")
                        });
                    mysql.save(function (err) {
                        if (err) {
                            console.log(err)
                        }
                        else {
                            console.log('saveo'+i)
                            i++
                        }
                    });




                }
            })


            setInterval(function () {

                conn.query('SELECT   G1_PVVoltage,G1_PVCurrent,G1_PVPower_kW,Wind_Speed_ms,Wind_Direction,G1_ACToday_notHold,DatetimeSMU1 FROM (SELECT G1_PVVoltage,G1_PVCurrent,G1_PVPower_kW,Wind_Speed_ms,Wind_Direction,G1_ACToday_notHold,DatetimeSMU1 FROM main_table ORDER BY DatetimeSMU1 DESC LIMIT 0,1)sub ORDER BY DatetimeSMU1 ASC', function (err, rows) {
                    if (err) {
                        console.log(err)
                    }
                    for (id in rows) {
                        // console.log(rows)
                        var dateformat = require('dateformat')
                     
                        var mysql = new Mysql({

                            currents: rows[id].G1_PVCurrent,
                            voltage: rows[id].G1_PVVoltage,
                            power: rows[id].G1_PVPower_kW,
                            energy: rows[id].G1_ACToday_notHold,
                            wind_speed: rows[id].Wind_Speed_ms,
                            wind_direction: rows[id].Wind_Direction,
                            timestamp:dateformat(rows[id].DatetimeSMU1.toString(),  "isoDateTime")
                        });

                        datas = JSON.stringify(rows[id])

                        let date_time = dateformat(rows[id].DatetimeSMU1.toString(),  "isoDateTime")
                        Mysql.find({}).sort({ _id: -1 }).limit(1)
                            .then((data) => {

                                if (date_time!= data[0].timestamp) {

                                    try {

                                        mysql.save(function (err) {
                                            if (err) {
                                                console.log(err)
                                            }
                                            else {
                                                console.log({ data: mysql })
                                            }
                                        });
                                    }
                                    catch (err) {
                                        console.log(err)
                                    }
                                }
                                else {
                                    console.log('.')

                                }

                            })
                            .catch((err) => {
                                return res.status(500).send(err);
                            })
                    }
                })





            }, 1000);
        }
        catch (err) {
            console.log(err)
        }
    });
};
MysqlController.find_hour =  function (res) {
 
        
     

        var id = 1;
        let y = 1
        let i = 0
        try {



            conn.query(`SELECT G1_ACToday_notHold,DatetimeSMU1 FROM main_table WHERE DATE_FORMAT( DatetimeSMU1 , '%Y-%m-%d' ) BETWEEN DATE_ADD( curdate( ) , INTERVAL -100 DAY ) AND CURDATE( ) GROUP BY  DATE( DatetimeSMU1), HOUR( DatetimeSMU1 )`, function (err, rows) {
                if (err) {
                    console.log(err);
                }
                for (id in rows) {


                     // console.log(rows)
                        var dateformat = require('dateformat')
                     
                        var mysql_hour = new Mysql_Hour({

                            energy: rows[id].G1_ACToday_notHold,
                            timestamp:dateformat(rows[id].DatetimeSMU1.toString(),  "isoDateTime")
                        });
                        mysql_hour.save(function (err) {
                        if (err) {
                            console.log(err)
                        }
                        else {
                            console.log('saveo'+i)
                            i++
                        }
                    });




                }
            })


            setInterval(function () {

                conn.query(`SELECT G1_ACToday_notHold,DatetimeSMU1 FROM main_table WHERE DATE_FORMAT( DatetimeSMU1 , '%Y-%m-%d' ) BETWEEN DATE_ADD( curdate( ) , INTERVAL -100 DAY ) AND CURDATE( ) GROUP BY  DATE( DatetimeSMU1), HOUR( DatetimeSMU1 ) ORDER BY DatetimeSMU1 DESC LIMIT 0,1`, function (err, rows) {
                    if (err) {
                        console.log(err)
                    }
                    for (id in rows) {
                        // console.log(rows)
                        var dateformat = require('dateformat')
                     
                        var mysql_hour = new Mysql_Hour({

                            energy: rows[id].G1_ACToday_notHold,
                            timestamp:dateformat(rows[id].DatetimeSMU1.toString(),  "isoDateTime")
                        });

                        datas = JSON.stringify(rows[id])

                        let date_time = dateformat(rows[id].DatetimeSMU1.toString(),  "isoDateTime")
                        Mysql_Hour.find({}).sort({ _id: -1 }).limit(1)
                            .then((data) => {

                                if (date_time!= data[0].timestamp) {

                                    try {

                                        mysql_hour.save(function (err) {
                                            if (err) {
                                                console.log(err)
                                            }
                                            else {
                                                console.log({ data: mysql })
                                            }
                                        });
                                    }
                                    catch (err) {
                                        console.log(err)
                                    }
                                }
                                else {
                                    console.log('.')

                                }

                            })
                            .catch((err) => {
                                return res.status(500).send(err);
                            })
                    }
                })





            }, 1000);
        }
        catch (err) {
            console.log(err)
        }
  
};

MysqlController.findG= function (req, res) {
    Mysql.find({}).sort({ _id: -1 }).limit(10)
        .then((data) => {

            res.status(201).send({
                status: "success",
                data: data

            })

        })
        .catch((err) => {
            console.log(err)
        })
}
MysqlController.findOne = function (req, res) {
    Mysql.find({}).sort({ _id: -1 }).limit(1)
        .then((data) => {

            res.status(201).send({
                status: "success",
                data: data

            })

        })
        .catch((err) => {
            console.log(err)
        })
}
MysqlController.findG_hour= function (req, res) {
    Mysql_Hour.find({
        "timestamp": {
            '$gte': req.body.day + "T00:00:00.000+00:00",
            '$lte': req.body.day + "T23:59:59.000+00:00"
        }
    }, {"timestamp":true ,"_id":false}).select("energy")
        .then((data) => {

            res.status(201).send({
                status: "success",
                data: data

            })

        })
        .catch((err) => {
            console.log(err)
        })
}
MysqlController.findOne_hour = function (req, res) {
    Mysql_Hour.find({}).sort({ _id: -1 }).limit(1)
        .then((data) => {

            res.status(201).send({
                status: "success",
                data: data

            })

        })
        .catch((err) => {
            console.log(err)
        })
}
MysqlController.findDay = function (req, res) {

    var cols=req.body.col
    console.log(cols)
    Mysql.find({
        "timestamp": {
            '$gte': req.body.day + "T00:00:00.000+00:00",
            '$lte': req.body.day + "T23:59:59.000+00:00"
        }
    }, {"timestamp":true ,"_id":false}).select(cols)
        .then((data) => {

            res.status(201).send({
                status: "success",
                data: data

            })

        })
        .catch((err) => {
            console.log(err)
        })
}

MysqlController.findMonth = function (req, res) {
    var now = new Date();
    var y = now.getFullYear().toString();
    console.log(parseInt(req.body.month) + 1)

    Mysql.find({
        "timestamp": {
            '$gte': y + "-" + "0" + req.body.month + "-" + "02" + "T00:00:00.000+00:00",
            '$lte': y + "-" + "0" + (parseInt(req.body.month) + 1).toString() + "-" + "01" + "T23:59:59.000+00:00"
        }
    })
        .then((data) => {

            res.status(200).send(data)

        })
        .catch((err) => {
            console.log(err)
        })
}
MysqlController.findYear = function (req, res) {
    Mysql.find({
        "timestamp": {
            '$gte': req.body.year + "-01-01T00:00:00.000+00:00",
            '$lte': (parseInt(req.body.year) + 1).toString() + "-01-01T00:00:00.000+00:00"
        }
    })
        .then((data) => {

            res.status(201).send({
                status: "success",
                data: data

            })

        })
        .catch((err) => {
            console.log(err)
        })
}
MysqlController.LastHour = function (req, res) {
    Mysql.find().reverse().limit(60)
        .then((data) => {

            res.status(201).send({
                status: "success",
                data: data

            })

        })
        .catch((err) => {
            console.log(err)
        })
}
module.exports = MysqlController

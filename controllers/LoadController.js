var Load = require("../model/load");
var LoadController = {};
LoadController.insert = function (req, res, next) {
    if(req!=undefined){
        var load = new Load({
            username:req.body.username,
            type:req.body.type,
            measure_watt:req.body.measure_watt,
            amount:req.body.amount,
            times_use_hr:req.body.times_use_hr
         
        });
        load.save(function (err) {
            if (err) {
                console.log(err)
            }
            else {
                res.status(201).send({
                    status: "success",
                    data: load
    
                })
            }
        });
    }
   
}
LoadController.find = function (req, res, next) {
    if(req!=undefined){
        Load.find( { username: req.body.username } )
        .then((data) => {

            res.status(201).send({
                status: "success",
                data: data

            })

        })
        .catch((err) => {
            console.log(err)
        })
    }
   
}
LoadController.del = function (req, res, next) {
    if(req!=undefined){
        console.log(req.body.id)
        var id=req.body.id
        Load.deleteOne({_id: id})
        .then( 

            res.status(201).send({
                status: "success",

            })

        )
        .catch((err) => {
            console.log(err)
        })
    }
   
}
module.exports = LoadController
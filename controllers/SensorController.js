var Sensor = require("../model/sensor");
var dateFormat = require('dateformat');
const fetch = require('node-fetch');
const Bluebird = require('bluebird');
fetch.Promise = Bluebird;
var sensorController = {};
var socketIO = require('socket.io')
let datas = []
sensorController.insert = function (req, res) {
    setInterval(function () {
        fetch('http://64.227.6.193:5000/save')
            .then(response => response.json())
            .then((data) => {
                var dateformat = require('dateformat')

                // console.log(data.p)
                var length = parseInt(data.p.length) - 1
                // console.log(data.p[length])
                var sensor = new Sensor({
                    temper: data.p[length].temperature,
                    humidi: data.p[length].humidity,
                    pm1: data.p[length].pm1,
                    pm2_5: data.p[length].pm2_5,
                    pm10: data.p[length].pm10,
                    timestamp: dateformat(new Date(),  "isoDateTime")


                })

                sensor.save(function (err) {
                    if (err) {
                        console.log(err)
                    }
                    else {
                        console.log({data: sensor })
                    }
                })




            });
    }, 60000);



};
sensorController.findG = function (req, res) {
    Sensor.find({}).sort({ _id: -1 }).limit(10)
        .then((data) => {

            res.status(201).send({
                status: "success",
                data: data

            })

        })
        .catch((err) => {
            console.log(err)
        })
}
sensorController.findOne = function (req, res) {
    Sensor.find({}).sort({ _id: -1 }).limit(1)
        .then((data) => {

            res.status(201).send({
                status: "success",
                data: data

            })

        })
        .catch((err) => {
            console.log(err)
        })
}
sensorController.findDay = function (req, res) {

    var cols=req.body.col
    Sensor.find({
        "timestamp": {
            '$gte': req.body.day + "T00:00:00.000+00:00",
            '$lte': req.body.day + "T23:59:59.000+00:00"
        }
    }, {"timestamp":true ,"_id":false}).select(cols)
        .then((data) => {

            res.status(201).send({
                status: "success",
                data: data

            })

        })
        .catch((err) => {
            console.log(err)
        })
}

sensorController.findMonth = function (req, res) {
    var now = new Date();
    var y = now.getFullYear().toString();
    console.log(parseInt(req.body.month) + 1)

    Sensor.find({
        "timestamp": {
            '$gte': y + "-" + "0" + req.body.month + "-" + "02" + "T00:00:00.000+00:00",
            '$lte': y + "-" + "0" + (parseInt(req.body.month) + 1).toString() + "-" + "01" + "T23:59:59.000+00:00"
        }
    })
        .then((data) => {

            res.status(200).send(data)

        })
        .catch((err) => {
            console.log(err)
        })
}
sensorController.findYear = function (req, res) {
    Sensor.find({
        "timestamp": {
            '$gte': req.body.year + "-01-01T00:00:00.000+00:00",
            '$lte': (parseInt(req.body.year) + 1).toString() + "-01-01T00:00:00.000+00:00"
        }
    })
        .then((data) => {

            res.status(201).send({
                status: "success",
                data: data

            })

        })
        .catch((err) => {
            console.log(err)
        })
}
module.exports = sensorController;


const jwt = require("jwt-simple");
const loginMiddleWare=require('./LoginController')

const passport = require("passport");
//ใช้ในการ decode jwt ออกมา
const ExtractJwt = require("passport-jwt").ExtractJwt;
//ใช้ในการประกาศ Strategy
const JwtStrategy = require("passport-jwt").Strategy;
var fs = require("fs");
console.log(loginMiddleWare())
const SECRET = "fvghgvj3543121";
//สร้าง Strategy
const jwtOptions = {
   jwtFromRequest: ExtractJwt.fromHeader("key"),
   secretOrKey: SECRET
};
const jwtAuth = new JwtStrategy(jwtOptions, (payload, done) => {
   if (payload.sub === loginMiddleWare()) done(null, true);
   else done(null, false);
});
//เสียบ Strategy เข้า Passport
passport.use(jwtAuth);
//ทำ Passport Middleware
const requireJWTAuth = passport.authenticate("jwt",{session:false});
//module.exports=requireJWTAuth,SECRET
module.exports =requireJWTAuth



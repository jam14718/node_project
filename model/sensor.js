var mongoose = require('../config/mongodb');
var dateFormat = require('dateformat');
var moment = require('moment-timezone');
var d = new Date()

var SensorSchema = new mongoose.Schema({
    timestamp: {type:String,default:d.toLocaleTimeString()},
    temper: {type:String,default:0},
    humidi: {type:String,default:0},
    pm1: {type:String,default:0},
    pm2_5: {type:String,default:0},
    pm10: {type:String,default:0},

  });
  
module.exports = mongoose.model('Sensor', SensorSchema,'solar_sensor');

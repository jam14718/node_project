var mongoose = require('mongoose');


var LoadSchema = new mongoose.Schema({
   username:String,
   type:String,
   measure_watt:Number,
   amount:Number,
   times_use_hr:Number

});

module.exports = mongoose.model('Load', LoadSchema,'solar_Load');
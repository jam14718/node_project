var mongoose = require('mongoose');
var datamsql=require('./../controllers/MysqlController')
var dateFormat = require('dateformat');

var MysqlSchema = new mongoose.Schema({
    currents: String,
    voltage: String,
    power: String,
    energy:String,
    radiation: String,
    wind_speed:String,
    wind_direction:String,
    timestamp: String,


});

module.exports = mongoose.model('Mysql', MysqlSchema,'solar_mysql');
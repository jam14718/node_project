let router = require('express').Router();
mysql =require('../controllers/MysqlController');
let requireJWTAuth=require('../controllers/JWT')
router.get('/', requireJWTAuth,function (req, res) {
    
    res.status(200).json({ status: "success", message: "successfully"});
});
router.get('/GetFM',requireJWTAuth,function(req,res){
    mysql.findOne(req,res)
})
router.get('/GetFM_G',requireJWTAuth,function(req,res){
    mysql.findG(req,res)
})
router.get('/GetFM_hour',requireJWTAuth,function(req,res){
    mysql.findOne_hour(req,res)
})
router.post('/GetFM_G_hour',function(req,res){
    mysql.findG_hour(req,res)
})
router.post('/GetFM_day',requireJWTAuth,function(req,res){
    mysql.findDay(req,res)
})
module.exports = router;
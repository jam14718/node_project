let router = require('express').Router();
sensor =require('../controllers/SensorController');
let requireJWTAuth=require('../controllers/JWT')
router.get('/', requireJWTAuth, function (req, res) {
    res.status(200).json({ status: "success", message: "successfully"});
});
router.get('/GetFS',requireJWTAuth, function(req,res){
    sensor.findOne(req,res)
})
router.get('/GetFS_G',requireJWTAuth, function(req,res){
    sensor.findG(req,res)
})
router.post('/GetFS_day',requireJWTAuth, function(req,res){
    sensor.findDay(req,res)
})
router.post('/GetFS_month',requireJWTAuth, function(req,res){
    sensor.findMonth(req,res)
})
router.post('/GetFS_year',requireJWTAuth, function(req,res){
    sensor.findYear(req,res)
})
module.exports = router;
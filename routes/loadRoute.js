let router = require('express').Router();
let load =require('../controllers/LoadController');
let requireJWTAuth=require('../controllers/JWT')
router.get('/', function (req, res) {
    
    res.status(200).json({ status: "success", message: "successfully"});
});
router.post('/insert', requireJWTAuth, function (req, res) {
    try{
        load.insert(req, res)

    }
    catch(err){
        res.status(500).json({ status: "fail", message: "can't insert"});

    }
});
router.post('/find', requireJWTAuth, function (req, res) {
    try{
        load.find(req, res)

    }
    catch(err){
        res.status(500).json({ status: "fail", message: "can't find"});

    }
});
router.post('/del', requireJWTAuth, function (req, res) {
    try{
        load.del(req, res)

    }
    catch(err){
        res.status(500).json({ status: "fail", message: "can't delete"});

    }
});
module.exports = router;